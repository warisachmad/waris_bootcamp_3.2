<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class bookingController extends Controller
{
    
    function bookingRoom(Request $request){
        DB::beginTransaction();

        try{
             $this->validate($request, [
                'checkin_date' => 'required',
                'checkout_date' => 'required',
                'payment_status' => 'required',
                'customer_id' => 'required',
                'room_id' => 'required' 
              ]);

            $checkin_date = $request->input('checkin_date');
            $checkout_date = $request->input('checkout_date');
            $payment_status = $request->input('payment_status');
            $customer_id = $request->input('customer_id');
            $room_id = $request->input('room_id');

            $book = new transaksi;
            $book->checkin_date = $checkin_date;
            $book->checkout_date = $checkout_date;
            $book->payment_status = $payment_status;
            $book->customer_id = $customer_id;
            $book->room_id = $room_id;
            $book->save();

            DB::commit();

            return response()->json(["message" => "Success !!!"], 200);
        }
        catch(\Exception $e){
            DB::rollback();

            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

}
